/*!
The `asuran-cli` binary provides a lightweight wrapper over the core `asuran`
logic, providing simple set of commands for directly interacting with
repositories.
 */
#[cfg(not(tarpaulin_include))]
mod cli;

#[cfg(not(tarpaulin_include))]
mod bench;
#[cfg(not(tarpaulin_include))]
mod contents;
#[cfg(not(tarpaulin_include))]
mod extract;
#[cfg(not(tarpaulin_include))]
mod list;
#[cfg(not(tarpaulin_include))]
mod new;
#[cfg(not(tarpaulin_include))]
mod store;

use anyhow::Result;
use cli::{Command, Opt};
use structopt::StructOpt;

cfg_if::cfg_if! {
    if #[cfg(feature = "jemallocator")] {
        #[global_allocator]
        static GLOBAL: jemallocator::Jemalloc = jemallocator::Jemalloc;
        const ALLOCATOR_NAME: &str = "jemalloc";
    } else if #[cfg(feature = "mimalloc")]{
        #[global_allocator]
        static GLOBAL: mimalloc::MiMalloc = mimalloc::MiMalloc;
        const ALLOCATOR_NAME: &str = "MiMalloc";
    } else {
        const ALLOCATOR_NAME: &str = "System Malloc";
    }
}

#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    smol::run(async {
        // Our task in main is dead simple, we only need to parse the options and
        // match on the subcommand
        let options = Opt::from_args();
        let command = options.command.clone();
        match command {
            Command::New { .. } => new::new(options).await,
            Command::Store { target, name, .. } => store::store(options, target, name).await,
            Command::List { .. } => list::list(options).await,
            Command::Extract {
                target,
                archive,
                glob_opts,
                preview,
                ..
            } => extract::extract(options, target, archive, glob_opts, preview).await,
            Command::BenchCrypto => bench::bench_crypto().await,
            Command::Contents {
                archive, glob_opts, ..
            } => contents::contents(options, archive, glob_opts).await,
        }
    })
}
